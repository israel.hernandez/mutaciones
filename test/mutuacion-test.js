const assert = require('assert');
const mutuacionController = require('../src/controllers/mutuacion-controller');

describe('Pruebas unitarias de mutuaciones de cadenas DNA', () => {

    it('Cadena DNA con mutuación vertical', () => {
        const result = mutuacionController.hasMutation(["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]);
        assert.equal(result, true);
    });

    it('Cadena DNA con mutuación diagonal', () => {
        const result = mutuacionController.hasMutation(["ATGCGA","CAGTGC","TTATGT","AGAAGG","ACCCTA","TCACTG"]);
        assert.equal(result, true);
    });

    it('Cadena DNA sin mutuación', () => {

        const result = mutuacionController.hasMutation(["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"]);
        assert.equal(result, false);
    });
})
