# Detectar mutuaciones en cadenas DNA

El código de este repositorio tiene como objetivo detectar si la cadena dada de DNA tiene mutuaciones o no.


# Tecnologías

La implementación de la solución fue desarrollada mediante la creación de una **Api** hecha en **nodo js**, en cuanto a la base de datos se utilizo **Cosmos DB** de **Azure**.

# Código
La api cuenta con dos métodos:

## mutation
Este método verifica si la cadena DNA tiene mutuación o no. Regresa un estatus **200** si la cadena tiene mutación y un estatus **403** si la cadena NO tiene mutación.

## stats
Este método regresa las estadísticas de la api. Nos regresa cuantas cadenas con mutación y sin mutación se han verificado y su proporcionalidad.

## Ejecución en ambiente local

Una vez clonado el repositorio, escribir el siguiente comando para correr el servicio: **npm run start** , y se desea correr pruebas: **npm run test** .

# Publicación

A modo de prueba, la solución esta implementada en **Azure**, se puede encontrar en [https://check-mutations.azurewebsites.net](https://check-mutations.azurewebsites.net)

## Ejecución

Para poder hacer peticiones a la Api, se requiere el uso de **Postman** o algún programa similar.

**Verificar mutación**

Method: **Post**

Content-type: **application/json**

URL: **https://check-mutations.azurewebsites.net/mutation**

Body:  **{
    "dna": ["ATCCTA","AAGTGC","TGTTGT","AGAAGG","CGCCTA","TCACTC"]
}**




**Estadísticas**

Method: **Post**

Content-type: **application/json**

URL: **https://check-mutations.azurewebsites.net/stats**




