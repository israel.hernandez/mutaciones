const app = require('./src/app');
const config = require('./src/db_context/config');
const dbContext = require('./src/db_context/database-context');
const CosmosClient = require("@azure/cosmos").CosmosClient;
const { endpoint, key, databaseId, containerId } = config;
const client = new CosmosClient({ endpoint, key });

// Make sure Tasks database is already setup. If not, create it.
async function main() {
    await dbContext.create(client, databaseId, containerId);
}

main();

app.listen(app.get('serverPort'), () => {
    console.log(`Server running on port ${app.get('serverPort')}`)
});
