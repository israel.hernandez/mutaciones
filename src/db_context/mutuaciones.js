const CosmosClient = require("@azure/cosmos").CosmosClient;
const config = require("./config");
const { endpoint, key, databaseId, containerId } = config;
const client = new CosmosClient({ endpoint, key });
const database = client.database(databaseId);
const container = database.container(containerId);

class Mutuaciones {

    constructor() {
    }

    async create(mutuacion) {
        const { resource: createdItem } = await container.items.create(mutuacion);

        return createdItem;
    }

    async get(){
        const querySpec = {
            query: "SELECT * from c"
        };
        
        const { resources: items } = await container.items
            .query(querySpec)
            .fetchAll();
        
        return items;
    }
    
}

module.exports = Mutuaciones;