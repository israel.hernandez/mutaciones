const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

// Settings
app.set('serverPort', process.env.PORT || 4000);

// Middleware
app.use(express.json());
app.use(bodyParser.json());
app.use(cors());

// Routes
app.use(require('./routes/mutuaciones-route'));


module.exports = app;