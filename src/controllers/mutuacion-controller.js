const secuenciaCuatroLetrasIgualesRegExp = new RegExp('A{4,}|T{4,}|C{4,}|G{4,}');
const Mutuaciones = require('../db_context/mutuaciones');

// ********************************
// Regresa verdadero, si la cadena DNA proporcionada tiene mutuación
// devuelve false en caso contrario
// ********************************
function hasMutation(dna) {
    const numeroRenglonesDNA = dna[0].length;
    const numeroColumnasDNA = dna.length;
    let secuenciasCuatroLetras = 0;
    let secuenciasEncontradas = '';

    // Comprueba secuencias horizontalmente en cada renglon
    for(let i=0; i<numeroColumnasDNA; i++) {
        if(tieneCuatroLetrasIgualesConsecutivas(dna[i])) {
            secuenciasCuatroLetras++;
            secuenciasEncontradas += dna[i] + ',';
        }

        if(secuenciasCuatroLetras > 1) {
            return {tieneMutacion: true, secuenciasEncontradas: secuenciasEncontradas};
        }
    }

    // Comprueba secuencias verticales en cada columna
    for(let col=0; col<numeroColumnasDNA; col++) {
        let columnaPivoteada = '';
        for(let row=0; row<numeroRenglonesDNA; row++) {
            columnaPivoteada += dna[row][col];
        }

        if(tieneCuatroLetrasIgualesConsecutivas(columnaPivoteada)) {
            secuenciasCuatroLetras++;
            secuenciasEncontradas += columnaPivoteada + ',';
        }

        if(secuenciasCuatroLetras > 1) {
            return {tieneMutacion: true, secuenciasEncontradas: secuenciasEncontradas};
        }
    }

    // Compruena secuencias diagonales derechas
    for(let row=0; row <= (numeroRenglonesDNA - 4); row++ ) {
        for(let col=0; col <= (numeroColumnasDNA - 4); col++) {
            if(row != 0 && col > 0)
                break;

            let x = col;
            let y = row;
            let diagonal = '';

            while( (x < numeroRenglonesDNA) && (y < numeroRenglonesDNA ) ) {
                diagonal += dna[y][x];
                x++;
                y++;
            }

            if(tieneCuatroLetrasIgualesConsecutivas(diagonal)) {
                secuenciasCuatroLetras++;
                secuenciasEncontradas += diagonal + ',';
            }
    
            if(secuenciasCuatroLetras > 1) {
                return {tieneMutacion: true, secuenciasEncontradas: secuenciasEncontradas};
            }
        }
    }

    // Compruena secuencias diagonales izquierdas
    for(let row=0; row <= (numeroRenglonesDNA - 4); row++ ) {
        for(let col=(numeroColumnasDNA - 1); col > (numeroColumnasDNA - 4) ; col--) {
            if(row != 0 && col < numeroColumnasDNA - 1)
                break;

            let x = col;
            let y = row;
            let diagonal = '';

            while( (x  >= 0) && (y < numeroRenglonesDNA) ) {
                diagonal += dna[y][x];
                x--;
                y++;
            }

            if(tieneCuatroLetrasIgualesConsecutivas(diagonal)) {
                secuenciasCuatroLetras++;
                secuenciasEncontradas += diagonal + ',';
            }
    
            if(secuenciasCuatroLetras > 1) {
                return {tieneMutacion: true, secuenciasEncontradas: secuenciasEncontradas};
            }
        }
    }

    return {tieneMutacion: false, secuenciasEncontradas: ''};
}

// ********************************
// Método que valida los valores de la matriz DNA
// Las letras de los Strings solo pueden ser: (A,T,C,G)
// ********************************
function validarValoresDNA(dna) {
    if(!Array.isArray(dna))
        return;
    
    const validationRegExp = new RegExp('^[A|T|C|G]+$');
    for(let i=0; i<dna.length; i++) {
        if(dna.length<=0 || !validationRegExp.test(dna[i]))
        {
            return false;
        }
    }

    return true;
}

// ********************************
// Método que valida secuencia de cuatro letras iguales consecutivas
// ********************************
function tieneCuatroLetrasIgualesConsecutivas(rowDNA) {
    if ( secuenciaCuatroLetrasIgualesRegExp.test(rowDNA) ) {
        console.log(rowDNA);
        return true;
    }

    return false;
}

// ********************************
// Método que persiste en base de datos una secuencia dna y su resultado
// ********************************
function persistirDNA(dna, dnaConMutacion) {
    const mutuaciones = new Mutuaciones();

    const nuevaMutuacion = {
        dna: dna,
        mutation: dnaConMutacion
    }

    mutuaciones.create(nuevaMutuacion);
}

module.exports = {
    hasMutation, 
    validarValoresDNA,
    persistirDNA
};