const { Router } = require('express');
const controller = require('../controllers/mutuacion-controller');
const Mutuaciones = require('../db_context/mutuaciones');
const router = Router();

// ********************************
// Devuelve status 200, si la cadena DNA no tiene mutuación
// devuelve 403, si la cadena DNA tiene mutuación
// devuelve 400, si hay error en los parámetros de entrada
// ********************************
router.post('/mutation', (req, res) => {

    // Validations
    if( !req.body.dna || !Array.isArray(req.body.dna)) {
        return res.status(400).send('Parámetros invalidos: no tiene el formato esperado.')
    }

    const dna = req.body.dna;
    if( !controller.validarValoresDNA(dna)) {
        return res.status(400).send('Parámetros invalidos: valores no permitidos.')
    }

    const numeroRenglonesDNA = dna[0].length;
    const numeroColumnasDNA = dna.length;
    for(let i=1; i<numeroColumnasDNA; i++) {
        if(dna[i].length != numeroRenglonesDNA) {
            return res.status(400).send('Parámetros invalidos: cadenas de longitudes diferentes.')
        }
    }

    // Encuentra mutuaciones
    const dnaHasMutation = controller.hasMutation(dna);
    controller.persistirDNA(dna, dnaHasMutation.tieneMutacion);

    if(dnaHasMutation.tieneMutacion) {
        return res.status(200).send(`Primeras secuencias encontradas: ${dnaHasMutation.secuenciasEncontradas}`);
    } else {
        return res.status(403).send();
    }
})

// ********************************
// Devuelve las estadisticas de las mutuaciones DNA
// ********************************
router.get('/stats', (req, res) => {
    const mutuaciones = new Mutuaciones();

    mutuaciones.get()
    .then(resp => {
        const dnaConMutacion = resp.filter(item => item.mutation);
        const dnaSinMutuacion = resp.filter(item => !item.mutation);

        return res.status(200).json({count_mutations: dnaConMutacion.length, count_no_mutation: dnaSinMutuacion.length, ratio: (dnaConMutacion.length/dnaSinMutuacion.length)});
    }, error => {
        return res.status(500).send(error.message);
    })
    .catch(error => {
        console.log(error);
        return res.status(500).send(error.message);
    })
})

module.exports = router;